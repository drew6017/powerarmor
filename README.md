# PowerArmor #
This is the official repository for the PowerArmor project. This project is based off of the mod Draconic Evolution and introduces very powerful armor and other tools. The twist is that it does this with zero mods and therefore is compatible with all vanilla clients. Are you a developer? Feel free to make a commit.

### Contribution guidelines ###

* Community standard code that is clean, efficient, and useful.
* Thorough testing before uploading anything, even if you think that there is no possibility of errors.
* Please use common sense for the rest.